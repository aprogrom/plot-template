#-------------------------------------------------
#
# Project created by QtCreator 2017-02-13T15:58:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = qt_graf_test
TEMPLATE = app


SOURCES += main.cpp\
        graphtest.cpp \
    qcustomplot.cpp \
    templatetask.cpp

HEADERS  += graphtest.h \
    qcustomplot.h \
    abstracttask.h \
    templatetask.h \
    abstractfunction.h

FORMS    += graphtest.ui
CONFIG += c++11
