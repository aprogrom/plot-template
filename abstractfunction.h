#ifndef ABSTRACTFUNCTION
#define ABSTRACTFUNCTION

#endif // ABSTRACTFUNCTION

class AbstractTask
{
public:
    virtual double calculate() = 0;
};
