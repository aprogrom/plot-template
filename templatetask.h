#ifndef TEMPLATETASK_H
#define TEMPLATETASK_H
#include "abstracttask.h"

class TemplateTask: public AbstractTask
{
private:
    QVector<double>* _draw_data_x;
    QVector<double>* _draw_data_y;
    double _start_X;
    double _end_X;
    double _h;
    bool _calculated = false;
public:
    TemplateTask(double start_X, double end_X, double h);
    ~TemplateTask();
    double function(double x);
    virtual QPair<const QVector<double>*,const QVector<double>*>* getDarwInfo();
    virtual void calculate();
    virtual bool calculated();
    virtual double getAxisRightBorder();
    virtual double getAxisLeftBorder();
    virtual double getAxisTopBorder();
    virtual double getAxisBotBorder();
};

#endif // TEMPLATETASK_H
