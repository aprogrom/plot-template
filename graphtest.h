#ifndef GRAPHTEST_H
#define GRAPHTEST_H

#include <QMainWindow>
#include "abstracttask.h"

namespace Ui {
class GraphTest;
}

class GraphTest : public QMainWindow
{
    Q_OBJECT

public:
    explicit GraphTest(QWidget *parent = 0, AbstractTask *task = 0);
    ~GraphTest();

private slots:
    void on_btn_calc_task_clicked();

    void on_btn_draw_task_clicked();

private:
    Ui::GraphTest* _ui;
    AbstractTask* _task;
};

#endif // GRAPHTEST_H
