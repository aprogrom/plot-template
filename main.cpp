#include "graphtest.h"
#include <QApplication>
#include "templatetask.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TemplateTask* task = new TemplateTask(-1,1,0.01);
    GraphTest w(0, task);
    w.show();

    return a.exec();
}
