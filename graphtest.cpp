#include "graphtest.h"
#include "ui_graphtest.h"


GraphTest::GraphTest(QWidget *parent, AbstractTask *task) :
    QMainWindow(parent),
    _ui(new Ui::GraphTest)
{
    _ui->setupUi(this);
    _task = task;
}

GraphTest::~GraphTest()
{
    delete _ui;
}


void GraphTest::on_btn_calc_task_clicked(){
    _task->calculate();
}

void GraphTest::on_btn_draw_task_clicked(){
    if(_task->calculated()){
        QPair<const QVector<double>*,const QVector<double>*>* draw_info = _task->getDarwInfo();
        _ui->qcp_graph->clearGraphs();
        _ui->qcp_graph->addGraph();
        _ui->qcp_graph->graph(0)->setData(*draw_info->first, *draw_info->second);
        _ui->qcp_graph->xAxis->setLabel("x");
        _ui->qcp_graph->yAxis->setLabel("y");
        _ui->qcp_graph->xAxis->setRange(_task->getAxisLeftBorder(), _task->getAxisRightBorder());//Для оси Ox
        _ui->qcp_graph->yAxis->setRange(_task->getAxisBotBorder(), _task->getAxisTopBorder());//Для оси Oy
        _ui->qcp_graph->replot();
    }else{
        QMessageBox msgBox;
        msgBox.setText("Not runing calculate");
        msgBox.exec();
    }
}
