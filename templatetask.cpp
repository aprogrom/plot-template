#include "TemplateTask.h"
#include <iostream>

TemplateTask::TemplateTask(double start_X, double end_X, double h){
    _start_X = start_X;
    _end_X = end_X;
    _h = h;
    int N = (end_X - start_X) / h;
//    std::cout << N;
    _draw_data_x = new QVector<double>(N);
    _draw_data_y = new QVector<double>(N);
}

TemplateTask::~TemplateTask(){
    delete _draw_data_x;
    delete _draw_data_y;
}

double TemplateTask::function(double x){
    return x * x;
}

QPair<const QVector<double> *,const QVector<double> *> *TemplateTask::getDarwInfo(){
    QPair<const QVector<double> *,const QVector<double> *>* draw_data =
            new QPair<const QVector<double> *,const QVector<double> *>(_draw_data_x, _draw_data_y);
    return draw_data;
}


void TemplateTask::calculate(){
    double cur_X = _start_X;
    std::cout << cur_X << std::endl;
    std::cout << _h << std::endl;
    std::cout << _end_X << std::endl;
    while(cur_X < _end_X){
        _draw_data_x->append(cur_X);
        double y = function(cur_X);
        _draw_data_y->append(y);
        cur_X += _h;
    }
    _calculated = true;
}

bool TemplateTask::calculated(){
    return _calculated;
}

double TemplateTask::getAxisRightBorder(){
    return _end_X;
}

double TemplateTask::getAxisLeftBorder(){
    return _start_X;
}

double TemplateTask::getAxisTopBorder(){
    double max = INT_MIN;
    for(const auto y: *_draw_data_y){
        if(max < y){
            max = y;
        }
    }
    return max + 1;
}

double TemplateTask::getAxisBotBorder(){
    double min = INT_MAX;
    for(const auto y: *_draw_data_y){
        if(min > y){
            min = y;
        }
    }
    return min - 1;
}


