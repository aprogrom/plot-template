#ifndef ABSTRACTTASK_H
#define ABSTRACTTASK_H
#include <QVector>
#include <QMap>
#include <string>
#include <QPair>

class AbstractTask
{
public:
    virtual QPair<const QVector<double>*,const QVector<double>*>* getDarwInfo() = 0;
    virtual double getAxisRightBorder() = 0;
    virtual double getAxisLeftBorder() = 0;
    virtual double getAxisTopBorder() = 0;
    virtual double getAxisBotBorder() = 0;
    virtual void calculate() = 0;
    virtual bool calculated() = 0;
};

#endif // ABSTRACTTASK_H
